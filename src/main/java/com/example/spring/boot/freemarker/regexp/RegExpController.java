package com.example.spring.boot.freemarker.regexp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.regex.Pattern;

/**
 * Created by Arkady on 14.08.2015.
 */

@Controller
public class RegExpController {

    @RequestMapping("/checkPassword.act")
    private String checkPassword(@RequestParam(value = "password", defaultValue = "") String password, ModelMap model) {
        if (password != null && !password.isEmpty()) {
            model.put("isMatchPolicy", isMatchPolicy(password));
        }
        return "password";
    }

    public static boolean isMatchPolicy(String password) {
        boolean isDigit = Pattern.compile("[0-9]").matcher(password).find();   // must contains one digit from 0-9
        boolean isLetter = Pattern.compile("[a-zA-ZåäöÅÄÖ]").matcher(password).find();   // must contain one letter from a-z or A-Z
        String regex = Pattern.quote("abcdefghijklmnopqrstuvwxyzåäöABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ0123456789!#$£%&¤()*+,-./:;<=>?@[\\]_{}");
        boolean isOther = Pattern.compile("[^" + regex + "]").matcher(password).find();    // mustn't contain other symbols
        boolean isLength = Pattern.compile(".{8,}").matcher(password).matches();  // length at least 8 characters
        return isDigit && isLetter && isLength && !isOther;
    }
}