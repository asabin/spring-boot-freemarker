<#-- @ftlvariable name="isMatchPolicy" type="java.lang.Boolean" -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>RegExp</title>
    </head>
    <body>
        <form action="checkPassword.act" method="post">
            <div>
                Possible chars:
                abcdefghijklmnopqrstuvwxyzåäöABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ0123456789!#$£%&¤()*+,-./:;<=>?@[\]_{}
            </div>
            <br>
        <#if isMatchPolicy??>
            <#if isMatchPolicy>
                <div style="color: green">Correct!</div>
            <#else>
                <div style="color: red">Incorrect!</div>
            </#if>
        </#if>
            <div>
                <input id="password" name="password" size="150">
                <input type="submit" value="Check"/>
            </div>
        </form>
    </body>
</html>